Vélo'v
	API 9 minimum (2.3) => 100% des téléphones Android
		Play Services
	Target API 23
		Permissions
	Theme DayNight
	Splash screen
	Listes
		Thread
		Volley
		Favoris
		RecyclerView
		Rafraichir
		Tri par numéro
		Affichage erreur
		Affichage status de la station
		Couleur en fonction de l'état de la station et des vélos disponibles
	Details
		Maps
		Autres détails
	Maps
		Cluster
		Zoom auto
		Titre
	Favoris
		Suppression
		Rafraichir
	Stations les plus proches
		Rayon de 400m
	Paramètres
		Récupération des contracts