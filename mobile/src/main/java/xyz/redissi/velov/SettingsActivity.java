package xyz.redissi.velov;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

import xyz.redissi.velov.jcdecaux.Contract;
import xyz.redissi.velov.jcdecaux.JCDecauxParser;
import xyz.redissi.velov.jcdecaux.JCDecauxRequest;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener
    {
        @Override
        public void onCreatePreferences(Bundle bundle, String s) {
            addPreferencesFromResource(R.xml.preference);
            final ListPreference preference = (ListPreference) findPreference(getString(R.string.pref_contact));
            preference.setOnPreferenceChangeListener(this);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final ProgressDialog[] progress = new ProgressDialog[1];

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progress[0] = new ProgressDialog(getContext());
                            progress[0].setTitle(getString(R.string.downloading));
                            progress[0].setCancelable(false);
                            progress[0].setCanceledOnTouchOutside(false);
                            progress[0].show();
                        }
                    });
                    MyApplication.getInstance(getContext()).addToRequestQueue(JCDecauxRequest.getContracts(Secret.API_JCDECAUX, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            ArrayList<Contract> contracts = JCDecauxParser.parseContracts(response);
                            final String entries[] = new String[contracts.size()];
                            final String entryValues[] = new String[contracts.size()];
                            for(int i = 0; i < contracts.size(); i++) {
                                entries[i] = contracts.get(i).getName() + " - " + contracts.get(i).getCommercial_name();
                                entryValues[i] = contracts.get(i).getName();
                            }


                            preference.setEntries(entries);
                            preference.setEntryValues(entryValues);
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                            String contract = preferences.getString(getString(R.string.pref_contact), getString(R.string.pref_contact_default));
                            if(Arrays.asList(entryValues).contains(contract)){
                                int index = Arrays.asList(entryValues).indexOf(contract);
                                preference.setSummary(entries[index]);
                            }
                            progress[0].dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }));



                }
            }).start();


        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object o) {
            if(preference instanceof ListPreference) {
                int index = Arrays.asList(((ListPreference) preference).getEntryValues()).indexOf((String) o);
                if(index != -1)
                    preference.setSummary(((ListPreference) preference).getEntries()[index]);
            }
            return true;
        }
    }

}
