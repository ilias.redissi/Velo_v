package xyz.redissi.velov;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import java.util.ArrayList;

import xyz.redissi.velov.jcdecaux.Station;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<Station> stations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(Color.parseColor("#88888888"));
            }
        }

        Intent intent = getIntent();
        if(intent.hasExtra(StationsListFragment.TAG_STATIONS))
            stations = (ArrayList<Station>) intent.getSerializableExtra(StationsListFragment.TAG_STATIONS);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        final ClusterManager<Station> mClusterManager;
        mClusterManager = new ClusterManager<>(this, mMap);

        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for(Station station : stations){
            mClusterManager.addItem(station);
            builder.include(station.getPosition());
        }

        mClusterManager.setRenderer(new MyClusterRenderer(this, mMap ,mClusterManager));

        LatLngBounds bounds = builder.build();

        mClusterManager.getMarkerCollection().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                System.out.println(marker.isInfoWindowShown());
                if(marker.isInfoWindowShown()) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + marker.getPosition().latitude + "," + marker.getPosition().longitude + "&mode=b");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                } else {
                    marker.showInfoWindow();
                }
                return true;
            }
        });

        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));
    }

    public class MyClusterRenderer extends DefaultClusterRenderer<Station> {

        public MyClusterRenderer(Context context, GoogleMap map,
                                 ClusterManager<Station> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(Station item, MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);

            markerOptions.title(item.getName());
            markerOptions.snippet(getString(R.string.available_bikes, item.getAvailable_bikes(), item.getBike_stands()));
                if(item.getStatus().equalsIgnoreCase(Station.Status.OPEN.toString())){
                    double percent = (double)item.getAvailable_bikes() / item.getBike_stands();
                    if(percent >= 0.5)
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)); //Vert
                    else
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)); //Orange
                } else if(item.getStatus().equalsIgnoreCase(Station.Status.CLOSED.toString())){
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)); //Rouge
                }
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Station> cluster, MarkerOptions markerOptions) {
            super.onBeforeClusterRendered(cluster, markerOptions);

            int stands = 0;
            int bikes = 0;

            for(Station station : cluster.getItems()){
                bikes += station.getAvailable_bikes();
                stands += station.getBike_stands();
            }

            markerOptions.title(getString(R.string.available_bikes, bikes, stands));
        }

        @Override
        protected void onClusterItemRendered(Station clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            //here you have access to the marker itself
        }
    }
}
