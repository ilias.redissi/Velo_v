package xyz.redissi.velov;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import xyz.redissi.velov.jcdecaux.Station;

public class DetailStationActivity extends AppCompatActivity implements OnMapReadyCallback {
    private Station mStation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_station);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        
        mStation = (Station) getIntent().getSerializableExtra(MainActivity.TAG_STATION);

        ImageView statusImageView = (ImageView) findViewById(R.id.statusImage);
        int color = Color.parseColor("#FFFFFF");
        try {
            color = Color.parseColor("#616161"); //Gris
            if(mStation.getStatus().equalsIgnoreCase(Station.Status.OPEN.toString())){
                color = Color.parseColor("#388E3C"); //Vert
            } else if(mStation.getStatus().equalsIgnoreCase(Station.Status.CLOSED.toString())){
                color = Color.parseColor("#D32F2F"); //Rouge
            }
        } catch (IllegalArgumentException e){
            e.printStackTrace();
            color = Color.parseColor("grey");
        } finally {
            if (statusImageView != null) {
                statusImageView.setColorFilter(color);
            }
        }
        TextView statusTextView = (TextView) findViewById(R.id.statusText);
        if (statusTextView != null) {
            statusTextView.setText(mStation.getStatus());
        }

        TextView address = (TextView) findViewById(R.id.address);
        if (address != null) {
            if(mStation.getAddress().equals(""))
                address.setText(getString(R.string.not_adsress));
            else
                address.setText(mStation.getAddress());
        }

        TextView bonus = (TextView) findViewById(R.id.bonus);
        if (bonus != null) {
            if(mStation.isBonus())
                bonus.setText(R.string.bonus_station);
            else
                bonus.setText(R.string.not_bonus_station);
        }

        TextView banking = (TextView) findViewById(R.id.banking);
        if (banking != null) {
            if(mStation.isBanking())
                banking.setText(R.string.payment_accepted);
            else
                banking.setText(R.string.payment_refused);
        }

        TextView available = (TextView) findViewById(R.id.available);
        if (available != null) {
            available.setText(getString(R.string.available_bikes, mStation.getAvailable_bikes(), mStation.getBike_stands()));
        }


        if (toolbar != null) {
            toolbar.setTitle(mStation.getName());
        }

        setSupportActionBar(toolbar);


        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = mStation.getPosition();
        MarkerOptions marker = new MarkerOptions();
        marker.position(latLng)
                .title(mStation.getName());
        if(mStation.getStatus().equalsIgnoreCase(Station.Status.OPEN.toString())){
            double percent = (double)mStation.getAvailable_bikes() / mStation.getBike_stands();
            if(percent >= 0.5)
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)); //Vert
            else
                marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)); //Orange
        } else if(mStation.getStatus().equalsIgnoreCase(Station.Status.CLOSED.toString())){
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)); //Rouge
        }
        googleMap.addMarker(marker);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
    }
}
