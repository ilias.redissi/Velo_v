package xyz.redissi.velov;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONArray;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import xyz.redissi.velov.jcdecaux.JCDecauxParser;
import xyz.redissi.velov.jcdecaux.JCDecauxRequest;
import xyz.redissi.velov.jcdecaux.Station;
import xyz.redissi.velov.jcdecaux.StationAdapter;

/**
 * @author Ilias
 */
public class StationsListFragment extends Fragment implements SearchView.OnQueryTextListener {
    private RecyclerView mRecyclerView;
    private StationAdapter mAdapter;
    private CoordinatorLayout mCoordinatorLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<Station> stations;
    private FloatingActionButton fab;
    private Snackbar mSnackbar;

    public static final String TAG_STATIONS = "stations";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stations_list_fragment, container, false);


        mCoordinatorLayout = (CoordinatorLayout) getActivity().getWindow().getDecorView().findViewById(R.id.coordinator);

        fab = (FloatingActionButton) getActivity().getWindow().getDecorView().findViewById(R.id.fab);


        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), MapsActivity.class);
                    intent.putExtra(TAG_STATIONS, stations);
                    startActivity(intent);
                }
            });
        }

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerStation);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), null));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new StationAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getStations();
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.blue,
                R.color.green,
                R.color.orange,
                R.color.red);



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getStations();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String contract = preferences.getString(getString(R.string.pref_contact), getString(R.string.pref_contact_default));
        toolbar.setTitle(contract);
    }

    private void getStations(){
        if(isOnline()) {
            fab.show();
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String contract = preferences.getString(getString(R.string.pref_contact), getString(R.string.pref_contact_default));
            MyApplication.getInstance(getContext()).addToRequestQueue(JCDecauxRequest.getStations(Secret.API_JCDECAUX, contract, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(final JSONArray response) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            VolleyLog.d("%s", response.toString());
                            stations = JCDecauxParser.parseStations(getContext(), response);
                            mSwipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.clear();
                                    mAdapter.addAll(stations);
                                    mSwipeRefreshLayout.setRefreshing(false);
                                }
                            });

                            if(mSnackbar != null)
                                mSnackbar.dismiss();
                        }
                    }).start();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    String message = "";
                    NetworkResponse networkResponse = error.networkResponse;
                    if(error.getMessage() != null)
                        message = error.getMessage();
                    else if(networkResponse != null) {
                        if(networkResponse.statusCode == HttpURLConnection.HTTP_BAD_REQUEST)
                            message = getString(R.string.error_400);
                        else if(networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN)
                            message = getString(R.string.error_403);
                    } else
                        message = getString(R.string.error_unknown);
                    mSnackbar = Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getStations();
                                }
                            });
                    mSnackbar.show();
                    VolleyLog.e("Error: %s", error.getMessage());
                }
            }));
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
            fab.hide();
            mSnackbar = Snackbar.make(mCoordinatorLayout, "No connection", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getStations();
                        }
                    });

            mSnackbar.show();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<Station> filteredModelList = filter(stations, newText);
        mAdapter.animateTo(filteredModelList);
        mRecyclerView.scrollToPosition(0);
        return true;
    }

    private List<Station> filter(List<Station> stations, String query) {
        query = query.toLowerCase();

        final List<Station> filteredModelList = new ArrayList<>();
        for (Station station : stations) {
            final String text = station.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(station);
            }
        }
        return filteredModelList;
    }
}
