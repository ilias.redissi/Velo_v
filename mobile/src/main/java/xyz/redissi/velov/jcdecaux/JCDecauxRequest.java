package xyz.redissi.velov.jcdecaux;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Ilias
 */
public class JCDecauxRequest {
    private static final String BASE_URL  = "https://api.jcdecaux.com/vls/v1";
    private static final String SEPARATOR = "/";
    private static final String URL_STATIONS = "stations";
    private static final String URL_CONTRACTS = "contracts";
    private static final String QUESTION_MASK = "?";
    private static final String PARAMETER_API = "apiKey";
    private static final String PARAMETER_CONTRACT = "contract";
    private static final String QUERY_DELIMITER = "&";
    private static final String EQUALS = "=";

    private static String getUrlStations(String api_key, String contract){
        return BASE_URL + SEPARATOR + URL_STATIONS + QUESTION_MASK + PARAMETER_API + EQUALS + api_key + QUERY_DELIMITER + PARAMETER_CONTRACT + EQUALS + contract;
    }

    private static String getUrlStations(String api_key){
        return BASE_URL + SEPARATOR + URL_STATIONS + QUESTION_MASK + PARAMETER_API + EQUALS + api_key;
    }

    private static String getUrlStation(String api_key, int number, String contract){
        return BASE_URL + SEPARATOR + URL_STATIONS + SEPARATOR + number + QUESTION_MASK + PARAMETER_API + EQUALS + api_key + QUERY_DELIMITER + PARAMETER_CONTRACT + EQUALS + contract;
    }

    private static String getUrlContracts(String api_key){
        return BASE_URL + SEPARATOR + URL_CONTRACTS + QUESTION_MASK + PARAMETER_API + EQUALS + api_key;
    }

    public static JsonArrayRequest getStations(String api_key, String contract, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener){
        return new JsonArrayRequest(getUrlStations(api_key, contract), (JSONArray)null, listener, errorListener);
    }

    public static JsonArrayRequest getStations(String api_key, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener){
        return new JsonArrayRequest(getUrlStations(api_key), (JSONArray)null, listener, errorListener);
    }

    public static JsonObjectRequest getStation(String api_key, int number, String contract, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener){
        return new JsonObjectRequest(getUrlStation(api_key, number, contract), null, listener, errorListener);
    }

    public static JsonArrayRequest getContracts(String api_key, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener){
        return new JsonArrayRequest(getUrlContracts(api_key), (JSONArray)null, listener, errorListener);
    }
}
