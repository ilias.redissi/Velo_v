package xyz.redissi.velov.jcdecaux;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ilias
 */
public abstract class FavoriteStation {
    public static final String TAG_FAVORITES = "xyz.redissi.velov.PREFERENCE_FAVORITES";

    public static void addStation(@NonNull Context context, Station station){
        List<Station> stations = getAll(context);

        if (stations == null)
            stations = new ArrayList<>();
        if(!stations.contains(station))
            stations.add(station);
        String favorites = encode(stations);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TAG_FAVORITES, favorites);
        editor.apply();
    }

    @Nullable
    public static List<Station> getAll(@NonNull Context context){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String favorites = sharedPref.getString(TAG_FAVORITES, "");
        return decode(favorites);
    }

    public static boolean isFavorite(@NonNull Context context, Station station) {
        List<Station> stations = getAll(context);
        return stations != null && stations.contains(station);
    }

    public static void removeStation(@NonNull Context context, Station station){
        List<Station> stations = getAll(context);

        if (stations != null) {
            stations.remove(station);
        }

        String favorites = encode(stations);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TAG_FAVORITES, favorites);
        editor.apply();
    }

    @Nullable
    private static List<Station> decode(String favorites){
        Gson gson = new Gson();
        Type type = new TypeToken<List<Station>>(){}.getType();
        return gson.fromJson(favorites, type);
    }

    private static String encode(List<Station> stations){
        Gson gson = new Gson();
        return gson.toJson(stations);
    }
}
