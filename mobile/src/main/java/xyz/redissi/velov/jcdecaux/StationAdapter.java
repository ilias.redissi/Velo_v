package xyz.redissi.velov.jcdecaux;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import xyz.redissi.velov.DetailStationActivity;
import xyz.redissi.velov.MainActivity;
import xyz.redissi.velov.R;

/**
 * @author Ilias
 */
public class StationAdapter extends RecyclerView.Adapter<StationAdapter.ViewHolder>{
    protected SortedList<Station> mDataset;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mNumero;
        public TextView mName;
        public TextView mAddress;
        public TextView mAvailable;
        public ImageButton mFavorite;
        public ViewHolder(View v) {
            super(v);
            mNumero = (TextView) v.findViewById(R.id.numero);
            mName = (TextView) v.findViewById(R.id.name);
            mAddress = (TextView) v.findViewById(R.id.address);
            mAvailable = (TextView) v.findViewById(R.id.available);
            mFavorite = (ImageButton) v.findViewById(R.id.favorite);

            mFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    boolean favorite = !mDataset.get(position).isFavorite();
                    mDataset.get(position).setFavorite(favorite);
                    if(favorite) {
                        mFavorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                        FavoriteStation.addStation(mContext, mDataset.get(position));
                    } else {
                        mFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        FavoriteStation.removeStation(mContext, mDataset.get(position));
                    }
                    mFavorite.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
                }
            });

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Intent intent = new Intent(v.getContext(), DetailStationActivity.class);
            intent.putExtra(MainActivity.TAG_STATION, mDataset.get(position));
            v.getContext().startActivity(intent);
        }
    }

    public  StationAdapter() {
        mDataset = new SortedList<>(Station.class, new SortedList.Callback<Station>() {
            @Override
            public int compare(Station o1, Station o2) {
                return o1.getNumber().compareTo(o2.getNumber());
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Station oldItem, Station newItem) {
                return oldItem.getNumber().equals(newItem.getNumber());
            }

            @Override
            public boolean areItemsTheSame(Station item1, Station item2) {
                return item1.getNumber() == item2.getNumber();
            }
        });
    }

    @Override
    public StationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.station_layout, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mNumero.setText(String.valueOf(mDataset.get(position).getNumber()));

        //Couleur pour le fond
        int color = Color.parseColor("#FFFFFF");
        try {
            color = Color.parseColor("#616161"); //Gris
            if(mDataset.get(position).getStatus().equalsIgnoreCase(Station.Status.OPEN.toString())){
                double percent = (double)mDataset.get(position).getAvailable_bikes() / mDataset.get(position).getBike_stands();
                if(percent >= 0.5)
                    color = Color.parseColor("#388E3C"); //Vert
                else
                    color = Color.parseColor("#F57C00"); //Orange
            } else if(mDataset.get(position).getStatus().equalsIgnoreCase(Station.Status.CLOSED.toString())){
                color = Color.parseColor("#D32F2F"); //Rouge
            }
        } catch (IllegalArgumentException e){
            e.printStackTrace();
            color = Color.parseColor("grey");
        } finally {
            holder.mNumero.getBackground().setColorFilter(color,
                    PorterDuff.Mode.SRC);
        }

        holder.mName.setText(mDataset.get(position).getName());
        if(mDataset.get(position).getAddress().equals(""))
            holder.mAddress.setHeight(0);
        else
            holder.mAddress.setText(mDataset.get(position).getAddress());
        holder.mAvailable.setText(holder.itemView.getResources().getString(
                R.string.available_bikes,
                mDataset.get(position).getAvailable_bikes(),
                mDataset.get(position).getBike_stands()
        ));
        if(mDataset.get(position).isFavorite())
            holder.mFavorite.setImageResource(R.drawable.ic_favorite_black_24dp);
        else
            holder.mFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        holder.mFavorite.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
    }

    public Station get(int position) {
        return mDataset.get(position);
    }

    public int add(Station item) {
        return mDataset.add(item);
    }

    public int indexOf(Station item) {
        return mDataset.indexOf(item);
    }

    public void updateItemAt(int index, Station item) {
        mDataset.updateItemAt(index, item);
    }

    public void addAll(List<Station> items) {
        mDataset.beginBatchedUpdates();
        for (Station item : items) {
            mDataset.add(item);
        }
        mDataset.endBatchedUpdates();
    }

    public void addAll(Station[] items) {
        addAll(Arrays.asList(items));
    }

    public boolean remove(Station item) {
        return mDataset.remove(item);
    }

    public Station removeItemAt(int index) {
        return mDataset.removeItemAt(index);
    }

    public void clear() {
        mDataset.beginBatchedUpdates();
        //remove items at end, to avoid unnecessary array shifting
        while (mDataset.size() > 0) {
            mDataset.removeItemAt(mDataset.size() - 1);
        }
        mDataset.endBatchedUpdates();
    }

    public void animateTo(List<Station> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
    }

    private void applyAndAnimateRemovals(List<Station> stations) {
        for (int i = mDataset.size() - 1; i >= 0; i--) {
            final Station model = mDataset.get(i);
            if (!stations.contains(model)) {
                removeItemAt(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Station> stations) {
        for (int i = 0, count = stations.size(); i < count; i++) {
            final Station model = stations.get(i);
            if (mDataset.indexOf(model) == SortedList.INVALID_POSITION) {
                add(model);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
