package xyz.redissi.velov.jcdecaux;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Ilias
 */
public class JCDecauxParser {

    public static Station parseStation(Context context, JSONObject object){
        try {
            Station station = new Station();

            station.setNumber(object.getInt(Station.TAG_NUMBER));
            station.setName(object.getString(Station.TAG_NAME));
            station.setAddress(object.getString(Station.TAG_ADDRESS));

            JSONObject positionObjet = object.getJSONObject(Station.TAG_POSITION);
            Station.Position position = new Station.Position();
            position.setLat(positionObjet.getDouble(Station.TAG_LAT));
            position.setLng(positionObjet.getDouble(Station.TAG_LNG));
            station.setPosition(position);

            station.setBanking(object.getBoolean(Station.TAG_BANGINK));
            station.setBonus(object.getBoolean(Station.TAG_BONUS));
            station.setStatus(object.getString(Station.TAG_STATUS));
            station.setContract_name(object.getString(Station.TAG_CONTRACT_NAME));
            station.setBike_stands(object.getInt(Station.TAG_BIKE_STANDS));
            station.setAvailable_bike_stands(object.getInt(Station.TAG_AVAILABLE_BIKE_STANDS));
            station.setAvailable_bikes(object.getInt(Station.TAG_AVAILABLE_BIKES));
            station.setLast_update(object.getInt(Station.TAG__LAST_UPDATE));

            station.setFavorite(FavoriteStation.isFavorite(context, station));

            return station;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Station> parseStations(Context context, JSONArray array){
        ArrayList<Station> stations = new ArrayList<>();
        for(int i = 0; i < array.length(); i++){
            try {
                Station station = parseStation(context, array.getJSONObject(i));
                if(station != null)
                    stations.add(station);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return stations;
    }

    public static ArrayList<Contract> parseContracts(JSONArray array){
        ArrayList<Contract> contracts = new ArrayList<>();
        for(int i = 0; i < array.length(); i++){

            try {
                JSONObject object = array.getJSONObject(i);

                Contract contract = new Contract();

                contract.setName(object.getString(Contract.TAG_NAME));
                contract.setCommercial_name(object.getString(Contract.TAG_COMMERCIAL_NAME));
                contract.setCountry_code(object.getString(Contract.TAG_COUNTRY_CODE));

                JSONArray cities = object.getJSONArray(Contract.TAG_CITIES);
                ArrayList<String> citiesList = new ArrayList<>();
                for(int j = 0; j < cities.length(); j++)
                    citiesList.add(cities.getString(j));
                contract.setCities(citiesList);

                contracts.add(contract);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return contracts;
    }
}
