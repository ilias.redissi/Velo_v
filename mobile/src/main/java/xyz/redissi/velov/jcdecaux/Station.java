package xyz.redissi.velov.jcdecaux;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.io.Serializable;

/**
 * @author Ilias
 */
public class Station implements Serializable, ClusterItem {
    private Integer number;
    private String name;
    private String address;
    private Position position;
    private boolean banking;
    private boolean bonus;
    private String status;
    private String contract_name;
    private int bike_stands;
    private int available_bike_stands;
    private int available_bikes;
    private int last_update;
    private boolean favorite = false;

    public enum Status {
        OPEN("OPEN"),
        CLOSED("CLOSED");

        private String name;

        Status(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    public static final String TAG_NUMBER = "number";
    public static final String TAG_NAME = "name";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_POSITION = "position";
    public static final String TAG_LAT = "lat";
    public static final String TAG_LNG = "lng";
    public static final String TAG_BANGINK = "banking";
    public static final String TAG_BONUS = "bonus";
    public static final String TAG_STATUS = "status";
    public static final String TAG_CONTRACT_NAME = "contract_name";
    public static final String TAG_BIKE_STANDS = "bike_stands";
    public static final String TAG_AVAILABLE_BIKE_STANDS = "available_bike_stands";
    public static final String TAG_AVAILABLE_BIKES = "available_bikes";
    public static final String TAG__LAST_UPDATE = "last_update";

    public static class Position implements Serializable {
        private double lat;
        private double lng;

        public Position() {
        }

        public Position(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public Station() {
    }

    public Station(int number, String name, String address, Position position, boolean banking, boolean bonus, String status, String contract_name, int bike_stands, int available_bike_stands, int available_bikes, int last_update) {
        this.number = number;
        this.name = name;
        this.address = address;
        this.position = position;
        this.banking = banking;
        this.bonus = bonus;
        this.status = status;
        this.contract_name = contract_name;
        this.bike_stands = bike_stands;
        this.available_bike_stands = available_bike_stands;
        this.available_bikes = available_bikes;
        this.last_update = last_update;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(position.lat, position.lng);
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isBanking() {
        return banking;
    }

    public void setBanking(boolean banking) {
        this.banking = banking;
    }

    public boolean isBonus() {
        return bonus;
    }

    public void setBonus(boolean bonus) {
        this.bonus = bonus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContract_name() {
        return contract_name;
    }

    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    public int getBike_stands() {
        return bike_stands;
    }

    public void setBike_stands(int bike_stands) {
        this.bike_stands = bike_stands;
    }

    public int getAvailable_bike_stands() {
        return available_bike_stands;
    }

    public void setAvailable_bike_stands(int available_bike_stands) {
        this.available_bike_stands = available_bike_stands;
    }

    public int getAvailable_bikes() {
        return available_bikes;
    }

    public void setAvailable_bikes(int available_bikes) {
        this.available_bikes = available_bikes;
    }

    public int getLast_update() {
        return last_update;
    }

    public void setLast_update(int last_update) {
        this.last_update = last_update;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if(o instanceof Station) {
            Station station = (Station) o;
            result = number.equals(station.number) && contract_name.equals(station.contract_name);
        }
        return result;
    }
}
