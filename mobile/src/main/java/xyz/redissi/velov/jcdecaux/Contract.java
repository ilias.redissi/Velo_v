package xyz.redissi.velov.jcdecaux;

import java.util.List;

/**
 * @author Ilias
 */
public class Contract {
    private String name;
    private List<String> cities;
    private String commercial_name;
    private String country_code;

    public static final String TAG_NAME = "name";
    public static final String TAG_CITIES = "cities";
    public static final String TAG_COMMERCIAL_NAME = "commercial_name";
    public static final String TAG_COUNTRY_CODE = "country_code";

    public Contract() {
    }

    public Contract(String name, List<String> cities, String commercial_name, String country_code) {
        this.name = name;
        this.cities = cities;
        this.commercial_name = commercial_name;
        this.country_code = country_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }

    public String getCommercial_name() {
        return commercial_name;
    }

    public void setCommercial_name(String commercial_name) {
        this.commercial_name = commercial_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }
}
