package xyz.redissi.velov;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import xyz.redissi.velov.jcdecaux.JCDecauxParser;
import xyz.redissi.velov.jcdecaux.JCDecauxRequest;
import xyz.redissi.velov.jcdecaux.Station;
import xyz.redissi.velov.jcdecaux.StationAdapter;

/**
 * @author Ilias
 */
public class NearestFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private StationAdapter mAdapter;
    private CoordinatorLayout mCoordinatorLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<Station> stations;
    private FloatingActionButton fab;
    private Snackbar mSnackbar;

    public static final String TAG_STATIONS = "stations";

    private static final int REQUEST_LOCATION = 178;

    private static final double RADIUS = 400;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stations_list_fragment, container, false);


        mCoordinatorLayout = (CoordinatorLayout) getActivity().getWindow().getDecorView().findViewById(R.id.coordinator);

        fab = (FloatingActionButton) getActivity().getWindow().getDecorView().findViewById(R.id.fab);


        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), MapsActivity.class);
                    intent.putExtra(TAG_STATIONS, stations);
                    startActivity(intent);
                }
            });
        }

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerStation);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), null));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new StationAdapter();
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getStations();
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.blue,
                R.color.green,
                R.color.orange,
                R.color.red);



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getStations();
    }

    private void getStations(){
        if(isOnline()) {
            fab.show();
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String contract = preferences.getString(getString(R.string.pref_contact), getString(R.string.pref_contact_default));
            MyApplication.getInstance(getContext()).addToRequestQueue(JCDecauxRequest.getStations(Secret.API_JCDECAUX, contract, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(final JSONArray response) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            VolleyLog.d("%s", response.toString());
                            stations = JCDecauxParser.parseStations(getContext(), response);
                            final ArrayList<Station> nearestStations = new ArrayList<>();

                            LatLng myPosition = getLocation();
                            if(myPosition != null){
                                LatLngBounds circle = toBounds(myPosition, RADIUS);
                                int nbStationsProches = 0;
                                for(Station station : stations){
                                    if(circle.contains(station.getPosition()))
                                        nearestStations.add(station);
                                }
                            }
                            stations = nearestStations;
                            mSwipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.clear();
                                    mAdapter.addAll(stations);
                                    mSwipeRefreshLayout.setRefreshing(false);
                                }
                            });

                            if(mSnackbar != null)
                                mSnackbar.dismiss();
                        }
                    }).start();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    String message = "";
                    NetworkResponse networkResponse = error.networkResponse;
                    if(error.getMessage() != null)
                        message = error.getMessage();
                    else if(networkResponse != null) {
                        if(networkResponse.statusCode == HttpURLConnection.HTTP_BAD_REQUEST)
                            message = getString(R.string.error_400);
                        else if(networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN)
                            message = getString(R.string.error_403);
                    } else
                        message = getString(R.string.error_unknown);
                    mSnackbar = Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getStations();
                                }
                            });
                    mSnackbar.show();
                    VolleyLog.e("Error: %s", error.getMessage());
                }
            }));
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
            fab.hide();
            mSnackbar = Snackbar.make(mCoordinatorLayout, "No connection", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getStations();
                        }
                    });

            mSnackbar.show();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Nullable
    public LatLng getLocation() {
        // Get the location manager
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar.make(mCoordinatorLayout, R.string.permission_location_message, Snackbar.LENGTH_LONG)
                        .setAction(R.string.activate, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestPermissions(
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        REQUEST_LOCATION);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);
            }
            return null;
        } else {
            Location location = locationManager.getLastKnownLocation(bestProvider);
            Double lat,lon;
            if(location != null) {
                try {
                    lat = location.getLatitude();
                    lon = location.getLongitude();
                    return new LatLng(lat, lon);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    getLocation();
            }
        }
    }

    public LatLngBounds toBounds(LatLng center, double radius) {
        LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
        LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 45);
        return new LatLngBounds(southwest, northeast);
    }
}
