package xyz.redissi.velov;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG_STATION = "station";

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String contract = preferences.getString(getString(R.string.pref_contact), getString(R.string.pref_contact_default));
        toolbar.setTitle(contract);
        setSupportActionBar(toolbar);
        
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getBaseContext(), MapsActivity.class);
                    startActivity(intent);
                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if(drawer != null)
            drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if(navigationView != null)
            navigationView.setNavigationItemSelectedListener(this);

        if (findViewById(R.id.fragment_container) != null && savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new StationsListFragment()).commit();

        PreferenceManager.setDefaultValues(this, R.xml.preference, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_list) {
            if (findViewById(R.id.fragment_container) != null &&
                    !(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof StationsListFragment)) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new StationsListFragment()).commit();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                String contract = preferences.getString(getString(R.string.pref_contact), getString(R.string.pref_contact_default));
                toolbar.setTitle(contract);
            }
        } else if (id == R.id.nav_favorites) {
            if (findViewById(R.id.fragment_container) != null &&
                    !(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof FavoriteListFragment)) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new FavoriteListFragment()).commit();
                toolbar.setTitle(getString(R.string.favorites));
            }
        } else if (id == R.id.nav_nearest) {
            if (findViewById(R.id.fragment_container) != null &&
                    !(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof NearestFragment)) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new NearestFragment()).commit();
                toolbar.setTitle(getString(R.string.nearest_stations));
            }
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);

            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
