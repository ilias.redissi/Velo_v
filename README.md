# Stations de vélos JCDecaux

## Description
Application développé dans le cadre d'un DUT Informatique à l'Université Claude Bernard Lyon 1 sur le site de Bourg-en-Bresse.

Application Android pour les stations de vélos  fourni par la compagnie JCDecaux.
L'application montre les stations de Lyon par défaut, mais l'utilisateur peut choisir sa ville dans les paramètres.

## Fonctionnalités
- Application compatible de la version 2.3.3 d'Android jusqu'à la version 6.0.1
- Affichage des stations, triés par identifiant avec la possibilité d'effectuer une recherche
- Affichage des détails lors d'un clic d'une station sur la liste
- Différentes couleurs pour les stations
	- Station ouverte
		- Vert : Dispose de plus de 50% de vélos disponibles pour la station
		- Orange : Dispose de moins de 50% de vélos disponibles pour la station
	- Station fermée
		- Rouge
- Gestion de favoris
- Stations les plus proches dans un rayon de 400m
- L'application possède le thème Jour/Nuit, c'est à dire que l'application sera dans un thème clair le jour et sombre la nuit. La détermination de la nuit se fait soit à partir de l'heure locale (par exemple à 22h, l'application passe en thème sombre), soit si l'utilisateur accepte la géolocalisation, l'application passera alors en thème sombre au coucher du soleil.